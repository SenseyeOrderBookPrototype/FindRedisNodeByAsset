# FindRedisNodeByAsset

##### Benchmark:
```bash
BenchmarkHash-4                                 100 000 000               10.2 ns/op             0 B/op          0 allocs/op
BenchmarkMapRedisNodeByAssetFinder_Find-4        50 000 000               33.3 ns/op             0 B/op          0 allocs/op
BenchmarkHashRedisNodeByAssetFinder_Find-4       50 000 000               34.7 ns/op             0 B/op          0 allocs/op
```