package main

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

const NodeCount uint = 7

func TestHash(t *testing.T) {
	assert.Equal(t, uint(131), Hash("AB"))
}

func BenchmarkHash(b *testing.B) {
	for i := 0; i < b.N; i++ {
		Hash("AB")
	}
}

func BenchmarkMapRedisNodeByAssetFinder_Find(b *testing.B) {
	assets := generateAssets()

	finder := &MapRedisNodeByAssetFinder{
		data: calculateAssetNodeMap(assets, NodeCount),
	}

	benchmarkHashRedisNodeByAssetFinder(b, finder, assets)
}

func BenchmarkHashRedisNodeByAssetFinder_Find(b *testing.B) {
	assets := generateAssets()

	finder := &HashRedisNodeByAssetFinder{
		count: NodeCount,
	}

	benchmarkHashRedisNodeByAssetFinder(b, finder, assets)
}

func benchmarkHashRedisNodeByAssetFinder(b *testing.B, finder RedisNodeByAssetFinder, assets []string) {
	b.Helper()

	length := len(assets)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		asset := assets[i%length]

		finder.Find(asset)
	}
}

func generateAssets() []string {
	from := []byte("A")[0]
	to := []byte("Z")[0]
	del := []byte("_")[0]

	result := make([]string, 0, 26*26/2)

	for i := from; i < to; i++ {
		for j := i + 1; j < to; j++ {
			result = append(result, string([]byte{i, from, from, del, j, to, to}))
		}
	}

	return result
}

func calculateAssetNodeMap(assets []string, nodeCount uint) map[string]uint {
	result := make(map[string]uint, len(assets))

	for _, asset := range assets {
		result[asset] = Hash(asset) % nodeCount
	}

	return result
}
