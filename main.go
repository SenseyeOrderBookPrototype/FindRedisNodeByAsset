package main

type (
	RedisNodeByAssetFinder interface {
		Find(asset string) uint
	}

	MapRedisNodeByAssetFinder struct {
		data map[string]uint
	}

	HashRedisNodeByAssetFinder struct {
		count uint
	}
)

func (f *MapRedisNodeByAssetFinder) Find(asset string) uint {
	return f.data[asset]
}

func (f *HashRedisNodeByAssetFinder) Find(asset string) uint {
	return Hash(asset) % f.count
}

func Hash(asset string) uint {
	items := []byte(asset)

	result := uint(0)

	for _, item := range items {
		result += uint(item)
	}

	return result
}
